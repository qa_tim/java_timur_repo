public class Vehicle implements Comparable<Vehicle>{
    protected String model;
    protected String producer;
    protected int age;
    public Vehicle(String vmodel, String vproducer, int vage){
        model=vmodel;
        producer=vproducer;
        age=vage;
    }
    public String getModel(){
        return model;
    }

    public String getProducer() {
        return producer;
    }
    public int getAge(){
        return age;
    }
    public void setModel(String vmodel){
        model=vmodel;
    }

    public void setProducer(String vproducer) {
        producer=vproducer;
    }
    public void setAge(int vage){
        age=vage;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "model='" + model + '\'' +
                ", producer='" + producer + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Vehicle o) {
        return this.getAge()-o.getAge();
    }
}
