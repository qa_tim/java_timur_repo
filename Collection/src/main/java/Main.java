import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Random;
import java.util.*;

public class Main {


    public static void main(String[] args) {
        Map<String,List<Vehicle>> mapVehicle=new HashMap<>();
        List<Vehicle> vehicles_list=new LinkedList<Vehicle>() ;
        read(vehicles_list);
        write(vehicles_list);

        Collections.sort(vehicles_list);
        for(Vehicle v:vehicles_list){
            if(!mapVehicle.containsKey(v.getProducer())){
                List<Vehicle> listProducer=new LinkedList<Vehicle>();
                listProducer.add(v);
                mapVehicle.put(v.getProducer(),listProducer);
            } else {
                List<Vehicle> l=mapVehicle.get(v.getProducer());
                l.add(v);
            }
        }

        long beginArrayList=System.currentTimeMillis();
        ArrayList<Integer> listInteger=new ArrayList<>(100500);
        for(int i=0;i<100500;i++){
            Random random=new Random();
            listInteger.add(random.nextInt());
        }
        System.out.format("%d%n",System.currentTimeMillis()-beginArrayList);

        long beginListLinked=System.currentTimeMillis();
        List<Integer> listLinkedInteger=new LinkedList<Integer>();
        for(int i=0;i<100500;i++){
            Random random=new Random();
            listLinkedInteger.add(random.nextInt());
        }
        System.out.format("%d%n",System.currentTimeMillis()-beginListLinked);


        long beginInsertion=System.currentTimeMillis();
        listLinkedInteger.add(listLinkedInteger.size()/2,12441);
        listLinkedInteger.add((listLinkedInteger.size()/2)+1,13441);
        System.out.format("%d%n",System.currentTimeMillis()-beginInsertion);

        Set<Integer> s=new TreeSet<>(listLinkedInteger);
        List<Integer> listNoDuplicate=new LinkedList<Integer>(s);
        System.out.println("Max "+Collections.max(listNoDuplicate));
        System.out.println("Min "+Collections.min(listNoDuplicate));

        long beginReverse=System.currentTimeMillis();
        Collections.reverse(listInteger);
        System.out.format("%d%n",System.currentTimeMillis()- beginReverse);

        long beginReversNoDup=System.currentTimeMillis();
        Collections.reverse(listNoDuplicate);
        System.out.format("%d%n",System.currentTimeMillis()-beginReversNoDup);


    }
    public static List<Vehicle> read (List<Vehicle> vehicles_list) {
        try {
            List<String> lines = Files.readAllLines(Paths.get("/Users/qa-intern/Downloads/L2_vehicale_data.txt"));
            String[] vehicle;
            for (String line : lines) {
                vehicle=line.split(":");
                if(vehicle[0].equals("CAR")){
                    String[] prs=vehicle[1].split(",");
                    Car car=new Car(prs[0],prs[1],Integer.parseInt(prs[2].trim()),prs[3]);
                    vehicles_list.add(car);
                }
                else {
                    String[] prs=vehicle[1].split(",");
                    Truck truck=new Truck(prs[0],prs[1],Integer.parseInt(prs[2].trim()),Long.parseLong(prs[3].trim()));
                    vehicles_list.add(truck);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vehicles_list;
    }
    public static void write(List<Vehicle> vehicles_list) {
        try {
            FileWriter fileWriterCars = new FileWriter("cars.txt", true);
            FileWriter fileWriterTrucks= new FileWriter("trucks.txt", true);
            for (Vehicle vcl : vehicles_list) {
                if (vcl instanceof Car) {
                    Car car = (Car) vcl;
                    System.out.println(car.toString());
                    fileWriterCars.write(car.toString() + '\n');
                } else {
                    Truck truck = (Truck) vcl;
                    System.out.println(truck.toString());
                    fileWriterTrucks.write(truck.toString()+ '\n');
                }
            }
            fileWriterCars.flush();
            fileWriterTrucks.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
