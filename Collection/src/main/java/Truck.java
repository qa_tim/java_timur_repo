public class Truck extends Vehicle {
    protected long tonnage;
    public Truck(String model,String producer,int age, long tonnage){
        super(model,producer,age);
        this.tonnage=tonnage;
    }
    public long getTonnage(){
        return tonnage;
    }
    public void setTonnage(long tonnage){
        this.tonnage=tonnage;
    }
    @Override
    public String toString(){
        return super.toString()+tonnage;
    }
}
