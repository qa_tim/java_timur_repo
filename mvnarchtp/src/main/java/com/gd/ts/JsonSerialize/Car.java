package com.gd.ts.JsonSerialize;

public class Car extends Vehicle {
    private String type;
    public Car(String model,String producer,int age, String cartype){
        super(model,producer,age);
        type=cartype;
    }
    public String getType(){
        return type;
    }
    public void setType(String cartype){
        type=cartype;
    }
    @Override
    public String toString(){
        return super.toString()+type;
    }
}