public enum WeekDays {
    Monday(1),
    Tuesday(2),
    Wednesday(3),
    Thursday(4),
    Friday(5),
    Saturday(6),
    Sunday(7);
    private  final int order;
    WeekDays(int number){
        order=number;
    }
    public int getOrder(){
        return order;
    }
}
