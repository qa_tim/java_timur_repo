import java.util.Scanner;

public class DaysWeek {
    private static final byte MIN=1;
    private static final byte MAX=7;
    public static void main(String[] args){
        System.out.println("Input a number from 1 to 7");
        Scanner sc=new Scanner(System.in);
        int number=sc.nextByte();
        if((number>MAX)||(number<MIN))
            throw new IllegalArgumentException("Incorrect number");
        defineDay(number);
    }
    public static void defineDay(int number){
        WeekDays day;
        for(WeekDays days:WeekDays.values()){
            if (days.getOrder()==number){
                day=days;
                switch (day){
                    case Monday:
                        System.out.println("Your day is "+ day.Monday.toString());
                        break;
                    case Tuesday:
                        System.out.println("Your day is "+ day.Tuesday.toString());
                        break;
                    case Wednesday:
                        System.out.println("Your day is "+ day.Wednesday.toString());
                        break;
                    case Thursday:
                        System.out.println("Your day is "+ day.Thursday.toString());
                        break;
                    case Friday:
                        System.out.println("Your day is "+ day.Friday.toString());
                        break;
                    case Saturday:
                        System.out.println("Your day is "+ day.Saturday.toString());
                        break;
                    case Sunday:
                        System.out.println("Your day is "+ day.Sunday.toString());
                        break;
                }
                break;
            }
        }
    }
}
