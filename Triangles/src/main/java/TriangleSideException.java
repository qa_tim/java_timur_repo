public class TriangleSideException extends Exception {
    public TriangleSideException(String message)
    {
        super(message);
    }
}
