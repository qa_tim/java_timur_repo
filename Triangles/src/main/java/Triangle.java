import java.util.Scanner;

public class Triangle {
    private static final int MIN=0;
    private static final int MAX=2079;
    public static void main(String[] args)
    {
        //Sides of a triangle can be only positive. So we accept range [1,2729], not [-1400,2729]
        try
        {
            Scanner sc = new Scanner(System.in);
            System.out.println("Input a-side of a triangle");
            int a = sc.nextInt();
            System.out.println("Input b-side of a triangle");
            int b = sc.nextInt();
            System.out.println("Input c-side of a triangle");
            int c = sc.nextInt();
            if((a>MAX)||(b>MAX)||(c>MAX))
                throw new TriangleSideException("Out of range [1,2729]");
            if((a<=MIN)||(b<=MIN)||(c<=MIN))
                throw new TriangleSideException("Sides can't be negative or equal to 0");
            if((a < b + c)&&(b < a + c)&&(c < a + b)) // Check that sum of two sides is bigger than length of one side is
            {
                if ((a==b)&&(b==c)&&(a==c)) {
                    System.out.println("The T. is equilateral (A==B==C)");
                    return;
                }
                if ((a!=b)&&(b!=c)&&(a!=c)) {
                    System.out.println("The T. is scalene (A!=B!=C)");
                    return;
                }
                if ((a==b)||(b==c)||(a==c)) {
                    System.out.println("The T. is isosceles (A==B!=C)");
                    return;
                }
            }
            else
                throw new TriangleSideException("Sum of two sides is bigger than length of one side is");
        }
        catch(TriangleSideException e)
        {
            System.out.println(e.getMessage());
        }
        catch(Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}
