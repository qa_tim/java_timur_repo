package com.Timur;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Main {


    public static void main(String[] args) {
	// write your code here
        List<Vehicle> vehicles_list=new LinkedList<Vehicle>() ;
        read(vehicles_list);
        write(vehicles_list);
    }
    public static List<Vehicle> read (List<Vehicle> vehicles_list) {
        try {
            List<String> lines = Files.readAllLines(Paths.get("/Users/qa-intern/Downloads/L2_vehicale_data.txt"));
            String[] vehicle;
            for (String line : lines) {
                vehicle=line.split(":");
                if(vehicle[0].equals("CAR")){
                    String[] prs=vehicle[1].split(",");
                    Car car=new Car(prs[0],prs[1],Integer.parseInt(prs[2].trim()),prs[3]);
                    vehicles_list.add(car);
                }
                else {
                    String[] prs=vehicle[1].split(",");
                    Truck truck=new Truck(prs[0],prs[1],Integer.parseInt(prs[2].trim()),Long.parseLong(prs[3].trim()));
                    vehicles_list.add(truck);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vehicles_list;
    }
    public static void write(List<Vehicle> vehicles_list) {
        try {
            FileWriter fw_c = new FileWriter("cars.txt", true);
            FileWriter fw_t = new FileWriter("trucks.txt", true);
            for (Vehicle vcl : vehicles_list) {
                if (vcl instanceof Car) {
                    Car car = (Car) vcl;
                    System.out.println(car.toString());
                    fw_t.write(car.toString() + '\n');
                } else {
                    Truck truck = (Truck) vcl;
                    System.out.println(truck.toString());
                    fw_c.write(truck.toString()+ '\n');
                }
            }
            fw_c.flush();
            fw_t.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
