import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class DaysWeek {
    private static final byte MIN=1;
    private static final byte MAX=7;
    public static void main(String[] args){
        try {
            int number;
            List<String> stringNumber = Files.readAllLines(Paths.get("In.txt"));
            number=Integer.parseInt(stringNumber.get(0));
            if((number>MAX)||(number<MIN))
                throw new IllegalArgumentException("Incorrect number");
            defineDay(number);
        }catch(IOException e){
            e.printStackTrace();
        }catch(IllegalArgumentException e){
            e.printStackTrace();
        }
    }
    public static void defineDay(int number) {
        try {
            FileWriter fileWriter = new FileWriter("Out.txt");
            WeekDays day;
            for (WeekDays days : WeekDays.values()) {
                if (days.getOrder() == number) {
                    day = days;
                    switch (day) {
                        case Monday:
                            fileWriter.write("Your day is " + day.Monday.toString());
                            fileWriter.close();
                            break;
                        case Tuesday:
                            fileWriter.write("Your day is " + day.Tuesday.toString());
                            fileWriter.close();
                            break;
                        case Wednesday:
                            fileWriter.write("Your day is " + day.Wednesday.toString());
                            fileWriter.close();
                            break;
                        case Thursday:
                            fileWriter.write("Your day is " + day.Thursday.toString());
                            fileWriter.close();
                            break;
                        case Friday:
                            fileWriter.write("Your day is " + day.Friday.toString());
                            fileWriter.close();
                            break;
                        case Saturday:
                            fileWriter.write("Your day is " + day.Saturday.toString());
                            fileWriter.close();
                            break;
                        case Sunday:
                            fileWriter.write("Your day is " + day.Sunday.toString());
                            fileWriter.close();
                            break;
                    }
                    break;
                }
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
