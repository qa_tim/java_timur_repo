import org.junit.Test;
import org.junit.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TicketUnit {
    private Ticket ticket;
    @Before
    public void init(){
        ticket=new Ticket();
    }
    @Test
    public void testLuckyTicket(){
        assertTrue(ticket.isTicketLucky("001001"));
    }
    @Test
    public void testNotLuckyTicket(){
        assertFalse(ticket.isTicketLucky("002001"));
    }
    @Test (expected = IllegalArgumentException.class)
    public void testIllegalTicketLessSixDigits(){
        ticket.isTicketLucky("03342");
    }
    @Test (expected = IllegalArgumentException.class)
    public void testIllegalTicketMoreSixDigits(){
        ticket.isTicketLucky("0334245");
    }
    @Test (expected = IllegalArgumentException.class)
    public void testIllegalTicketNotDigit(){
        ticket.isTicketLucky("03342g");
    }
    @Test (expected = NullPointerException.class)
    public void testIllegalTicketNull(){
        ticket.isTicketLucky(null);
    }
    @Test (expected = IllegalArgumentException.class)
    public void testIllegalTicketEmpty(){
        ticket.isTicketLucky("");
    }

}
