public class Ticket {
    public static void main(String[] args)
    {
        String tn = args[0];
        try {
            if (isTicketLucky(tn))
                System.out.println("Lucky!");
            else
                System.out.println("Not lucky.");
        }catch(IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
    }

    public static boolean isTicketLucky(String tn)
    {
        tn.trim();
        if ((tn.length() > 6) || (tn.length() < 6))
            throw new IllegalArgumentException("Ticket can't have more/less 6 digits");
        String pt1 = tn.substring(0, 3);
        String pt2 = tn.substring(3, 6);
        int sum1 = 0;
        int sum2 = 0;
        for (int i = 0; i < 3; i++)
        {
            sum1 += Integer.parseInt(String.valueOf(pt1.charAt(i)));
            sum2 += Integer.parseInt(String.valueOf(pt2.charAt(i)));
        }
        return !(sum1 == sum2);
    }
}
