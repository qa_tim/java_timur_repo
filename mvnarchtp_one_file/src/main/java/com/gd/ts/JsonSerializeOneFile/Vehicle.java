package com.gd.ts.JsonSerializeOneFile;

import java.io.Serializable;

public class Vehicle implements Serializable {
    private String model;
    private String producer;
    private int age;
    public Vehicle(String vmodel, String vproducer, int vage){
        model=vmodel;
        producer=vproducer;
        age=vage;
    }
    public String getModel(){
        return model;
    }

    public String getProducer() {
        return producer;
    }
    public int getAge(){
        return age;
    }
    public void setModel(String vmodel){
        model=vmodel;
    }

    public void setProducer(String vproducer) {
        producer=vproducer;
    }
    public void setAge(int vage){
        age=vage;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "model='" + model + '\'' +
                ", producer='" + producer + '\'' +
                ", age=" + age +
                '}';
    }
}