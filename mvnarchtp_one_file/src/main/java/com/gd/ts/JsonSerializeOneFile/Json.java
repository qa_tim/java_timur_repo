package com.gd.ts.JsonSerializeOneFile;

import com.google.gson.Gson;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Json {
    public static void main(String[] args) {

            // write your code here
            List<Vehicle> vehicles_list = new LinkedList<Vehicle>();
            read(vehicles_list);
            write(vehicles_list);
            deserialize();
        }
    public static List<Vehicle> read (List<Vehicle> vehicles_list) {
        try {
            List<String> lines = Files.readAllLines(Paths.get("data.txt"));
            String[] vehicle;
            for (String line : lines) {
                vehicle = line.split(":");
                if ("CAR".equals(vehicle[0])) {
                    String[] prs = vehicle[1].split(",");
                    Car car = new Car(prs[0], prs[1], Integer.parseInt(prs[2].trim()), prs[3]);
                    vehicles_list.add(car);
                } else {
                    String[] prs = vehicle[1].split(",");
                    Truck truck = new Truck(prs[0], prs[1], Integer.parseInt(prs[2].trim()), Long.parseLong(prs[3].trim()));
                    vehicles_list.add(truck);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vehicles_list;
    }
    public static void write(List<Vehicle> vehicles_list) {
        try {
            FileWriter fileWriter= new FileWriter("Vehicles.json", true);;
            for (Vehicle vcl : vehicles_list) {
                Gson gson=new Gson();
                String jsonStr=gson.toJson(vcl);
                fileWriter.write(jsonStr+"\n");
                fileWriter.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void deserialize() {
        try {
            Gson gson = new Gson();
            FileReader fileReader = new FileReader("Vehicles.json");
            Scanner scan = new Scanner(fileReader);
            while(scan.hasNextLine()){
                String jsonStr = scan.nextLine();
                if(jsonStr.contains("type")) {
                    Car c = gson.fromJson(jsonStr, Car.class);
                }
                else {
                    Truck t = gson.fromJson(jsonStr, Truck.class);
                }
            }
            fileReader.close();
            scan.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

